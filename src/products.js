
export const products = [
  {
    id: 123,
    name: 'Książka CSS',
    price: 65.99,
    tax: 23,
    promo: false,
    dataAdded: new Date(2020, 1, 13),
    categories: ['css', 'html']
  }, {
    id: 234,
    name: 'Książka JavaScript',
    price: 97.99,
    tax: 23,
    promo: true,
    dataAdded: new Date(2020, 1, 20),
    categories: ['javascript']
  }, {
    id: 345,
    name: 'Książka HTML',
    price: 54.99,
    tax: 23,
    promo: false,
    dataAdded: new Date(2020, 1, 15),
    categories: ['html', 'css']
  }, {
    id: 456,
    name: 'Książka React',
    price: 124.99,
    tax: 23,
    promo: true,
    dataAdded: new Date(2020, 1, 20),
    categories: ['javascript', 'react']
  }
];

export default products
