/* eslint-disable no-redeclare */

var productsData = window.products
var cartItems = window.cartItems

// var productsListDiv = document.getElementById('productsList')
// var cartItemsListDiv = document.getElementById('cartItemsList')


const products = new CatalogueView('#productsList')
      products.loadData(productsData)



const cart = new ShoppingCartView('#cartItemsList')
      cart.loadData(cartItems)

      cart.addProduct(product)











// Clear Products List
renderProductsList(productsData, productsListDiv)

renderCart(cartItems, cartItemsListDiv)


// ======== FUNCTIONS ============

function renderCart(cartItems, cartItemsListDiv) {
  cartItemsListDiv.innerHTML = ''
  var totalPrice = 0

  for (var i in cartItems) {
    var item = cartItems[i]

    totalPrice += productGrossPrice(item.product) * item.amount

    var itemDiv = createCartDiv(item)
    cartItemsListDiv.append(itemDiv)
  }
  document.querySelector('.cart-total .cart-total-amount').innerText = totalPrice.toFixed(2)
  return totalPrice
}

function renderProductsList(productsData, productsListDiv) {
  productsListDiv.innerText = ''
  // Build Products List
  for (var i in productsData) {
    var currentProduct = productsData[i]
    var listItemDiv = createProductDiv(currentProduct)
    productsListDiv.appendChild(listItemDiv)
  }
  return i
}


function createCartDiv(item) {
  var product = item.product
  var grossPrice = productGrossPrice(product)

  // Cena
  var itemPrice =
    `${grossPrice.toFixed(2)} x ${item.amount} = ${(grossPrice * item.amount).toFixed(2)}`

  var wrapperDiv = document.createElement('div')

  wrapperDiv.innerHTML = `<div class="list-group-item">
      <strong class="product-name">${product.name}</strong>
      <div class="product-price float-right"> ${itemPrice} zł</div>
  </div>`

  var itemDiv = wrapperDiv.firstChild

  return itemDiv
}

function productGrossPrice(product) {
  return (product.price * (1 + product.tax / 100))
}

function createProductDiv(product, grossPrice) {
  var datetxt = product.dataAdded.toLocaleDateString('pl')
  var grossPrice = productGrossPrice(product).toFixed(2)

  var listItemDiv = document.createElement('div')

  listItemDiv.innerHTML = `<div class="list-group-item" 
                                data-product-id="${product.id}">

    ${product.name} - ${(product.promo ? 'PROMOCJA - ' : '')}

    <strong class="float-right"> ${grossPrice} PLN  </strong> 

    <p>${datetxt}</p>

    <input type="button" value="Do koszyka!" class="js-add-to-cart float-right btn btn-success">

  </div>`

  var button = listItemDiv.querySelector('.js-add-to-cart')
  button.addEventListener('click', addToCartClick)

  return listItemDiv.firstElementChild
}


function addToCartClick(event) {
  var productEl = event.target.closest('[data-product-id]')
  var productId = productEl.dataset.productId

  // Pobierz produkt
  console.log(getProductById(productId))

  // Dodaj do koszyka produkt

  // Przelicz sume w koszyku
}

function getProductById(searchID) {
  return products.find(function (product) {
    return (product.id == searchID)
  });
}
