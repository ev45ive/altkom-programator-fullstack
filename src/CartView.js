// export class View {
//     constructor(selector) {
//         this.el = document.querySelector(selector)
//         if (!this.el) { throw Error('Cant find element with selector ' + selector) }
//     }
// }
// export class CartView extends View { /* ... */

export class CartView {

    constructor(selector) {
        this.el = document.querySelector(selector)
        this.listEl = this.el.querySelector('.cartView_list')
        this.totalAmountEl = this.el.querySelector('.cart-total-amount')

        // Cart Positions ( product + amount + subtotal)
        this.items = []
    }

    updateTotal() {
        const total = this.items.reduce((total, item) => total + item.subtotal, 0)
        this.totalAmountEl.innerHTML = total.toFixed(2)
    }

    addProduct(product) {

        let item = this.items.find(item => {
            return product.id === item.product.id
        })

        if (item) {
            item.amount += 1;
            item.subtotal = product.price * item.amount
        } else {
            const amount = 1;

            item = {
                product,
                amount,
                subtotal: product.price * amount
            }

            this.items.push(item)
        }
        this.updateTotal()
        this.saveItemToServer(item)
    }

    render() {
        this.listEl.innerHTML = ''
        this.items
            .sort((a, b) => a.subtotal >= b.subtotal ? 1 : -1)
            .forEach(item => {
                const itemView = new CartItemView(item)
                // itemView.render()
                // this.listEl.append(itemView.el)
                itemView.appendTo(this.listEl)
            })
    }

    saveItemToServer(item) {
        fetch('http://localhost:3000/cart_items/', {
            method: 'POST',
            mode: 'cors',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                // id: item.product.id,
                productId: item.product.id,
                amount: item.amount,
                subtotal: item.subtotal
            })
        }).then(resp => resp.json())
            .then(item => console.log(item))
    }
}
// ===========

export class CartItemView {

    constructor(item) {
        this.item = item;

        this.el = document.createElement('div')
        this.el.classList.add('list-group-item');
    }

    render() {
        const { product, amount, subtotal } = this.item

        this.el.innerHTML = ` 
            <h4>${product.name} </h4>
            <span class="float-right"> 
            ${product.price} x ${amount} = 
            <strong> ${subtotal.toFixed(2)} </strong> </span>`
    }

    appendTo(parentEl) {
        this.render()
        parentEl.append(this.el)
    }
}