// import ThisWillBeDefaultExport from "./ProductsListView.js";
import { ProductsListView, DEFAULT_TITLE } from "./ProductsListView.js";


import { CartView } from "./CartView.js";

/* Cart */
const cartView = new CartView('#cartView')
// cartView.addProduct(products[0])
// cartView.addProduct(products[1])
// cartView.addProduct(products[1])
cartView.render()

window.cartView = cartView

/* Products */
const productsView = new ProductsListView('#productsList')

/* Products list "is talking to" Cart  */
productsView.addToCart = (product) => {
    cartView.addProduct(product)
    cartView.render()
}

productsView.loadProducts([])
productsView.render()

window.productsView = productsView

fetch('http://localhost:3000/products')
    .then(resp => resp.json())
    .then(products => {

        productsView.loadProducts(products)
        productsView.render()
    })

