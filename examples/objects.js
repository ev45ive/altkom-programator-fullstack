function makePerson(name) {
    var person = {
        name: name
    }
    return person;
}
var alice = makePerson('Alice')
var bob = makePerson('Bob')
// === 
var sayHello = function (person) {
    console.log('Hi, my name is ' + person.name)
}

// ==========

var personFunctions = {
    sayHello: function (person) {
        console.log('Hi, my name is ' + person.name)
    },
    /* sayGoodbye: funciotn... */
    /* ...: funciotn... */
}
// ==========

function makePerson(name) {
    var person = {
        name: name,
        sayHello: function (person) {
            console.log('Hi, my name is ' + person.name)
        }
    }
    return person;
}
var alice = makePerson('Alice')
var bob = makePerson('Bob')

alice.sayHello(alice)


// ==========
function makePerson(name) {
    var person = {
        name: name,
        sayHello: function () {
            console.log('Hi, my name is ' + person.name)
        }
    }
    return person;
}
var alice = makePerson('Alice')
var bob = makePerson('Bob')

alice.sayHello()
bob.sayHello()


// ==========

function makePerson(name) {
    var person = {
        name: name,
        sayHelloTo: function (friend) {
            person.lastFriendITalkedTo = friend;
            console.log('Hi ' + friend.name + ', my name is ' + person.name)
        }
    }
    return person;
}
var alice = makePerson('Alice')
var bob = makePerson('Bob')

alice.sayHelloTo(bob)
bob.sayHelloTo(alice)
alice.lastFriendITalkedTo === bob

// ==========

function WhatIsThis() {
    console.log(this);
}

WhatIsThis()
// VM15993:2 Window {parent: Window, opener: null, top: Window, length: 0, frames: Window, …}


window.WhatIsThis
// ƒ WhatIsThis(){
//     console.log(this); 
// }


var obj = { name: 'placki', funkcja: WhatIsThis }

obj.funkcja()
// VM15993:2 {name: "placki", funkcja: ƒ}

// ==========


function sayHelloTo(friend) {
    console.log('Hi ' + friend.name + ', my name is ' + this.name)
}

function makePerson(name) {
    var person = {
        name: name,
        sayHelloTo: sayHelloTo
    }
    return person;
}
var alice = makePerson('Alice')
var bob = makePerson('Bob')

alice.sayHelloTo(bob)
bob.sayHelloTo(alice)

alice.sayHello === bob.sayHello

// ==========
function SendMessage(from, to, subject, body, attachement) {
    console.log('Mesage:', from, to, subject, body, attachement)
}
// undefined
SendMessage('Zbyszek', 'Alice', 'Lubie...', 'Placki!!! :P', '[Placki img]')
// VM16973:2 Mesage: Zbyszek Alice Lubie... Placki!!! :P [Placki img]


// ==========

function Mailbox(from) {
    var mailbox = {
        from: from,
        send: SendMessage
    }
    return mailbox;
}

function SendMessage(to, subject, body = '', attachement = '') {
    console.log('Mesage:', this.from, to, subject, body, attachement)
}

var myBox = Mailbox('Zbyszek@placki')
myBox.send('Alice@wonder', 'Hello')


/// ====


function sayHelloTo(friend) {
    console.log('Hi ' + friend.name + ', my name is ' + this.name)
}

function Person(name) {
    this.name = name,
        this.sayHelloTo = sayHelloTo
}
var alice = new Person('Alice')
var bob = new Person('Bob')

/// ====

function Mailbox(from) {
    this.from = from,
        this.send = SendMessage
}

function SendMessage(to, subject, body = '', attachement = '') {
    console.log('Mesage:', this.from, to, subject, body, attachement)
}

var myBox = new Mailbox('Zbyszek@placki')
myBox.send('Alice@wonder', 'Hello')

var aliceBox = new Mailbox('Alice@wonder')
aliceBox.send('Zbyszek@placki', 'Stop spamming me!')


/// ====

var prototyp = { id: null, text: 'placki' }
var a = Object.create(prototyp)
var b = Object.create(prototyp)

b.text
"placki"

a.text
"placki"

b.id = 'b'
"b"

a.id
null

/// ====

function Person(name) {
    this.name = name
}

Person.prototype.sayHelloTo = function (friend) {
    console.log('Hi ' + friend.name + ', my name is ' + this.name)
}

var alice = new Person('Alice')
var bob = new Person('Bob')

alice.sayHelloTo(bob)

/// 

alice
// Person {name: "Alice"}name: "Alice"__proto__: sayHelloTo: ƒ (friend)constructor: ƒ Person(name)__proto__: Object
bob
// Person {name: "Bob"}name: "Bob"__proto__: sayHelloTo: ƒ (friend)constructor: ƒ Person(name)__proto__: Object
alice.sayHelloTo === bob.sayHelloTo
// true
alice.__proto__ === bob.__proto__
// true

/// ====

div.__proto__
// HTMLDivElement {Symbol(Symbol.toStringTag): "HTMLDivElement", constructor: ƒ}
div.__proto__.__proto__
// HTMLElement {…}
div.__proto__.__proto__.__proto__
// Element {…}
div.__proto__.__proto__.__proto__.__proto__
// Node {ELEMENT_NODE: 1, ATTRIBUTE_NODE: 2, TEXT_NODE: 3, CDATA_SECTION_NODE: 4, ENTITY_REFERENCE_NODE: 5, …}nodeType: (...)nodeName: (...)baseURI: (...)isConnected: (...)ownerDocument: (...)parentNode: (...)parentElement: (...)childNodes: (...)firstChild: (...)lastChild: (...)previousSibling: (...)nextSibling: (...)nodeValue: (...)textContent: (...)ELEMENT_NODE: 1ATTRIBUTE_NODE: 2TEXT_NODE: 3CDATA_SECTION_NODE: 4ENTITY_REFERENCE_NODE: 5ENTITY_NODE: 6PROCESSING_INSTRUCTION_NODE: 7COMMENT_NODE: 8DOCUMENT_NODE: 9DOCUMENT_TYPE_NODE: 10DOCUMENT_FRAGMENT_NODE: 11NOTATION_NODE: 12DOCUMENT_POSITION_DISCONNECTED: 1DOCUMENT_POSITION_PRECEDING: 2DOCUMENT_POSITION_FOLLOWING: 4DOCUMENT_POSITION_CONTAINS: 8DOCUMENT_POSITION_CONTAINED_BY: 16DOCUMENT_POSITION_IMPLEMENTATION_SPECIFIC: 32hasChildNodes: ƒ hasChildNodes()getRootNode: ƒ getRootNode()normalize: ƒ normalize()cloneNode: ƒ cloneNode()isEqualNode: ƒ isEqualNode()isSameNode: ƒ isSameNode()compareDocumentPosition: ƒ compareDocumentPosition()contains: ƒ contains()lookupPrefix: ƒ lookupPrefix()lookupNamespaceURI: ƒ lookupNamespaceURI()isDefaultNamespace: ƒ isDefaultNamespace()insertBefore: ƒ insertBefore()appendChild: ƒ appendChild()replaceChild: ƒ replaceChild()removeChild: ƒ removeChild()constructor: ƒ Node()Symbol(Symbol.toStringTag): "Node"get nodeType: ƒ nodeType()get nodeName: ƒ nodeName()get baseURI: ƒ baseURI()get isConnected: ƒ isConnected()get ownerDocument: ƒ ownerDocument()get parentNode: ƒ parentNode()get parentElement: ƒ parentElement()get childNodes: ƒ childNodes()get firstChild: ƒ firstChild()get lastChild: ƒ lastChild()get previousSibling: ƒ previousSibling()get nextSibling: ƒ nextSibling()get nodeValue: ƒ nodeValue()set nodeValue: ƒ nodeValue()get textContent: ƒ textContent()set textContent: ƒ textContent()__proto__: EventTarget
div.__proto__.__proto__.__proto__.__proto__.__proto__
// EventTarget {Symbol(Symbol.toStringTag): "EventTarget", addEventListener: ƒ, removeEventListener: ƒ, dispatchEvent: ƒ, constructor: ƒ}addEventListener: ƒ addEventListener()removeEventListener: ƒ removeEventListener()dispatchEvent: ƒ dispatchEvent()constructor: ƒ EventTarget()Symbol(Symbol.toStringTag): "EventTarget"__proto__: Object
div.__proto__.__proto__.__proto__.__proto__.__proto__.__proto__
// {constructor: ƒ, __defineGetter__: ƒ, __defineSetter__: ƒ, hasOwnProperty: ƒ, __lookupGetter__: ƒ, …}constructor: ƒ Object()__defineGetter__: ƒ __defineGetter__()__defineSetter__: ƒ __defineSetter__()hasOwnProperty: ƒ hasOwnProperty()__lookupGetter__: ƒ __lookupGetter__()__lookupSetter__: ƒ __lookupSetter__()isPrototypeOf: ƒ isPrototypeOf()propertyIsEnumerable: ƒ propertyIsEnumerable()toString: ƒ toString()valueOf: ƒ valueOf()toLocaleString: ƒ toLocaleString()get __proto__: ƒ __proto__()set __proto__: ƒ __proto__()
div.__proto__.__proto__.__proto__.__proto__.__proto__.__proto__.__proto__
// null

/// ====

function Person(name) {
    this.name = name
}

Person.prototype.sayHelloTo = function (friend) {
    console.log('Hi ' + friend.name + ', my name is ' + this.name)
}

function Employee(name, salary) {
    //     this.name = name
    Person.call(this, name, salary)
    this.salary = salary
}
Employee.prototype = Object.create(Person.prototype)
Employee.prototype.work = function () {
    console.log('Sorry, I need my ' + this.salary)
}

var alice = new Person('Alice')
var bob = new Person('Bob')
var tom = new Employee('Tom', 2000)



tom
// Employee {name: "Tom", salary: 2000}name: "Tom"salary: 2000__proto__: Personwork: ƒ ()__proto__: sayHelloTo: ƒ (friend)constructor: ƒ Person(name)__proto__: Object
tom.sayHelloTo(alice)
// VM20053:6 Hi Alice, my name is Tom
tom.work()
// VM20053:16 Sorry, I need my 2000



/// ====

class Person {

    constructor(name) {
        this.name = name
    }

    sayHelloTo(friend) {
        console.log('Hi ' + friend.name + ', my name is ' + this.name)
    }
}

class Employee extends Person {

    constructor(name, salary) {
        super(name)
        this.salary = salary
    }

    sayHello(){
        super.sayHelloTo({name:'I am busy '})
    }

    work() {
        console.log('Sorry, I need my ' + this.salary)
    }
}

var alice = new Person('Alice')
var bob = new Person('Bob')
var tom = new Employee('Tom', 2000)

tom
//  Employee {name: "Tom", salary: 2000}name: "Tom"salary: 2000__proto__: Personconstructor: class Employeework: ƒ work()__proto__: constructor: class PersonsayHelloTo: ƒ sayHelloTo(friend)__proto__: Object