
// ====
tablica = [1,2,3,4,5]

parzyste = [] 

tablica.forEach(function(x, index, all){
    if(x % 2 == 0){
        parzyste.push(x)
    }
});

// ====
tablica = [1,2,3,4,5]

parzyste = tablica.filter(function(x, index, all){
    return (x % 2 == 0)
});

parzyste
// ====
tablica = [1,2,3,4,5]

parzyste = tablica.find(function(x, index, all){
    return (x % 2 == 0)
});

parzyste
// ====
tablica = [1,2,3,4,5]

razy3 = tablica.map(function(x, index, all){
    return x * 3
});

razy3
// (5) [3, 6, 9, 12, 15]

// ====
tablica = [1,2,3,4,5]

tablica.reduce( function( sum, x, index, all){
    console.log(sum,x,index,all )
    
    return sum + x;
}, 0 )
// VM710:4 0 1 0 (5) [1, 2, 3, 4, 5]
// VM710:4 1 2 1 (5) [1, 2, 3, 4, 5]
// VM710:4 3 3 2 (5) [1, 2, 3, 4, 5]
// VM710:4 6 4 3 (5) [1, 2, 3, 4, 5]
// VM710:4 10 5 4 (5) [1, 2, 3, 4, 5]
// 15
// ====

var options = [
    function(){ return ('Jimmy') },
    function(){ return ('Johny') },
    function(){ return ('Jack') },
];

var makeFunction = function(){
    var pos = Math.floor( Math.random() * options.length )
    return options[ pos ];
}

var fun = makeFunction()
console.log('My friend is ' + fun() );  


// ====
setTimeout(function(){
    console.log('No hej!')

},20 * 1000)
7
clearTimeout(7)

// ====

setInterval(function(){
    console.log('No hej!')

},2 * 1000)
8
// 8VM1723:2 No hej!
clearInterval(8)

// === 

var options = [
    function(){ return ('Jimmy') },
    function(){ return ('Johny') },
    function(){ return ('Jack') },
];

var makeFunction = function(){
    var pos = Math.floor( Math.random() * options.length )
    return options[ pos ];
}

setInterval(function(){
    var fun = makeFunction()
    console.log('Today my best friend is ' + fun() ); 
},1000)

// === 

var isEven = function(x){ return x % 2 === 0}
// var isOdd = function(x){ return x % 2 !== 0}

var isNot = function( Fn ){ 

    return function isNotFn(x){
        var result = Fn(x)
        return !result
    }
}

tablica.filter( isNot( isEven ) );

// (3) [1, 3, 5]

// === 

// var isEven = function(x){ return x % 2 === 0}

// var isEven = (x,y) => { return x % 2 === 0}

// var isEven = x => { return x % 2 === 0}

var isEven = x => x % 2 === 0; 

// var isOdd = function(x){ return x % 2 !== 0}

var isNot = Fn => x => !Fn(x);

// y = (x) => x**2 + x*2 + 2

tablica.filter( isNot( isEven ) );


// === 

function combine( f, g ){

    return function(x){

        return f( g( x ) )
    }
}

Even = x => x % 2 == 0;
isNot = x => !x;


[1,2,3,4,5].filter( combine( isNot, Even ) )

// (3) [1, 3, 5]

// ===

