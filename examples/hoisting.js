// Hoisted var and function
// var z
// function abc ...

if( false ){
    var z = 123;
}


console.log(z);

abc()

function abc(){ console.log('abc') }


////

// var abc = undefined

abc()

var abc = function(){ console.log('abc') }

// abc == function ...



////

if( true ){
    // const z = 123;
    let z = 123;
}

console.log(z);