const tax  = .23, sklep_discount = .10;


function getPrice(product, discount /* = sklep_discount */){
    if(discount === undefined){
        discount = sklep_discount;
    }
    const price = product.price 
    let result = addTax(price)
        result = addDiscount(result, discount)
    return result 
}

function addTax(price){
    const priceWithTax = price * (1 + tax)
    return priceWithTax
}

function addDiscount(price, discount){
    const priceWithDiscount = price * (1 - discount)
    return priceWithDiscount
}

debugger;

var finalPrice = getPrice({ price: 100 }, 0.15) 
console.log(finalPrice);