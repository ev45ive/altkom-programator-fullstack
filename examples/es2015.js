

sum = (  ...pozostale ) =>  pozostale.reduce( (sum,x) => sum + x , 0 ) 

sum(1,2,3,4,5)
// 15

// ===
tablica = [1,2,3,4,5] 

sum( ...tablica )
// 15

// ===